<!DOCTYPE html>
<html>
<head>
    <title>INDEXNYA</title>
</head>
<body>
    <table>
        <thead>
        <th>Nama</th>
        <th>Banyak Halaman</th>
        <th>Jenis Buku</th>
        </thead>
        <tbody>
            @foreach($data_buku as $buku)
            <tr>
                <td>{{$buku->nama_buku}}</td>
                <td>{{$buku->banyak_halaman}}</td>
                <td>{{$buku->jenis_buku}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>