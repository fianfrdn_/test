<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

	<div class="container">
		<div class="card mt-5">
			<div class="card-body">
				
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>Nama Pengarang</th>
							<th>Buku</th>
							<th width="15%" class="text-center">Jumlah Buku</th>
						</tr>
					</thead>
					<tbody>
						@foreach($pengarang as $a)
						<tr>
							<td>{{ $a->nama }}</td>
							<td>
								@foreach($a->buku as $t)
									{{$t->nama_buku}},
								@endforeach
							</td>
							<td class="text-center">as</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>

</body>
</html>