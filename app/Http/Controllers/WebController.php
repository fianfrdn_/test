<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
 
use App\Pengarang;
 
class WebController extends Controller
{
   public function index(){
   	 $pengarang = Pengarang::all();
    	 return view('pengarang',['pengarang' => $pengarang]);
   }
}